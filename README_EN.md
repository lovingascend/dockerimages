English|[中文](README.md)

# Docker Images<a name="EN-US_TOPIC_0235606902"></a>

This branch is a docker image of version 20.0.0 and above. If you need to use version 1.32.0.0, please switch to the [1.32.0.0](https://gitee.com/lovingascend/dockerimages/tree/1.32.0.0/) branch.

After determining the required image, click the corresponding tag to go to the image folder and follow the instructions in the  **readme** file. The following describes the three Docker images.

1. [**Ascend-Toolkit-20.0.0.RC1**](https://gitee.com/lovingascend/dockerimages/tree/master/atlas200dk_light)

   20.0.0.RC1 version development environment docker image. Mindstudio and ADK have been installed in the environment, directly open Mindsutido to use.