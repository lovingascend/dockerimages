中文|[English](README_EN.md)

### dockerimages

此分支为20.0.0及以上版本docker镜像，如果需要使用1.32.0.0版本，请切换分支为[1.32.0.0](https://gitee.com/lovingascend/dockerimages/tree/1.32.0.0/)分支。
    
确定好需要的镜像后，点击下列对应标签或者进入对应文件夹，按照readme操作即可。

1. [**Ascend-Toolkit-20.0**](https://gitee.com/lovingascend/dockerimages/tree/master/Ascend-Toolkit-20.0)

    20.0版本开发环境docker镜像，按照readme操作即可使用。

2. [**Ascend-Toolkit-20.1**](https://gitee.com/lovingascend/dockerimages/tree/master/Ascend-Toolkit-20.1)

    20.1版本开发环境docker镜像，按照readme操作即可使用。
